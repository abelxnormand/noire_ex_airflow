import requests
from zipfile import ZipFile
from io import BytesIO
import pandas as pd
from datetime import timedelta
from datetime import datetime

from airflow import DAG
from airflow.operators.python import PythonOperator

TOP_1M_DOMAINS = 'http://s3.amazonaws.com/alexa-static/top-1m.csv.zip'
TOP_1M_DOMAINS_FILE = 'top-1m.csv'

default_args = {
    'owner': 'a.Noire',
    'depends_on_past': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'start_date': datetime(2021, 11, 15),
    'schedule_interval': '0 14 * * *'
}

#Таски 
def get_data():
    top_doms = requests.get(TOP_1M_DOMAINS, stream=True)
    zipfile = ZipFile(BytesIO(top_doms.content))
    top_data = zipfile.read(TOP_1M_DOMAINS_FILE).decode('utf-8')

    with open(TOP_1M_DOMAINS_FILE, 'w') as f:
        f.write(top_data)


def domain_airflow_rank():
    top_data_rank = pd.read_csv(TOP_1M_DOMAINS_FILE, names=['ranks', 'domain'])
    dm = 'airflow.com'
    place = top_data_rank[top_data_rank.domain==dm]
    if place.empty:
        return "Такого домена нет"
    else:
        return f'rank = {place.iloc[0].ranks}'

def domain_top_10():
    top_data_d = pd.read_csv(TOP_1M_DOMAINS_FILE, names=['rank', 'domain'])
    top_data_d['domain_zone'] = top_data_d.domain.apply(lambda x: x.split('.')[-1])
    top_domain = top_data_d.groupby('domain_zone', as_index=False)             .agg({'domain': 'count'}).rename(columns={'domain': 'quantity'})             .sort_values(by='quantity', ascending=False).head(10)
    with open('top_domain.csv', 'w') as f:
        f.write(top_domain.to_csv(index=False))

def domain_len_name():
    top_data_name = pd.read_csv(TOP_1M_DOMAINS_FILE, names=['rank', 'domain'])
    max_len = top_data_name.domain.str.len().sort_values(ascending=False).iloc[0]
    top_doms_len = top_data_name[top_data_df.domain.str.len()==max_len].sort_values(by='domain').head(1)     .iloc[0].domain
    return top_doms_len

# передаем глобальную переменную airflow
def print_data(ds):
    with open('top_domain.csv', 'r') as f:
        all_data = f.read()
    date = ds
    print(f'Top domains zone for date {date}')
    print(all_data)
    
    print(f'The longest domain for date {date}')
    print(domain_len_name())

    print()
    
    print(f'The place of airflow.com for date {date}')
    print(domain_airflow_rank())

#Инициализируем таски
dag = DAG('a.Noire', default_args=default_args)

t1 = PythonOperator(task_id='get_data',
                    python_callable=get_data,
                    dag=dag)

t2 = PythonOperator(task_id='domain_top_10',
                    python_callable=domain_top_10,
                    dag=dag)

t3 = PythonOperator(task_id='domain_len_name',
                    python_callable=domain_len_name,
                    dag=dag)

t4 = PythonOperator(task_id='domain_airflow_rank',
                    python_callable=domain_airflow_rank,
                    dag=dag)

t5 = PythonOperator(task_id='print_data',
                    python_callable=print_data,
                    dag=dag)

#порядок выполнения
t1 >> [t2, t3, t4] >> t5






